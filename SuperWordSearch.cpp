#include <iostream>
#include <string>

using namespace std;

int main()
{
    int position[8][2] = {{-1,0},{-1,-1},{0,-1},{1,-1},{1,0},{1,1},{0,1},{-1,1}};

    int row_num,column_num;
    cin>>row_num>>column_num;

    //keep a copy of grid
    char **base_grid = new char*[row_num];
    for(int i=0;i<row_num;++i){
        base_grid[i] = new char[column_num]; 
    }
    //calculate grid
    char **grid = new char*[row_num];
    for(int i=0;i<row_num;++i){
        grid[i] = new char[column_num]; 
    }

    for(int i = 0;i < row_num ; i ++ ){
        for(int j = 0;j < column_num ; j ++ ){
            cin>>base_grid[i][j];
        }
    }

    string mode;
    cin>>mode;

    int word_num;
    cin>>word_num;

    string *word = new string[word_num];
    for(int i = 0;i < word_num ; i ++ ){
        cin>>word[i];
    }

    //////////////////////////////////////////////////////////////////////////
    cout<<row_num<<" "<<column_num<<endl;;
    for(int i = 0;i < row_num ; i ++ ){
        for(int j = 0;j < column_num ; j ++ ){
            cout<<base_grid[i][j];
        }
        cout<<endl;
    }
    cout<<mode<<endl;
    cout<<word_num<<endl;
    for(int i = 0;i < word_num ; i ++ ){
        cout<<word[i]<<endl;
    }
    //////////////////////////////////////////////////////////////////////////

    for(int i = 0;i < word_num ; i ++ ){
        //calculate grid, pass value
        for(int r = 0;r < row_num ; r ++ ){
            for(int c = 0;c < column_num ; c ++ ){
                grid[r][c] = base_grid[r][c];
            }
        }
        //check the first character's index
        bool ch0_find = false;
        int ch0_row_num,ch0_column_num;
        for( ch0_row_num = 0;ch0_row_num < row_num ; ){
            for( ch0_column_num = 0;ch0_column_num < column_num ; ){
                if(grid[ch0_row_num][ch0_column_num] == word[i][0]){
                    grid[ch0_row_num][ch0_column_num] = '\0';
                    ch0_find = true;
                    break;
                }else{
                    ch0_column_num ++;
                }
            }
            if( ch0_find == true){
                break;
            }else{
                ch0_row_num++;
            }
    	}
        //not found the 0 character
        if( ch0_find == false ){
            cout<<"NOT FOUND"<<endl;
            continue;
        }
        // the word length is 1
        if( ch0_find == true && word[i].size() == 1 ){
            cout<<"("<<ch0_row_num<<","<<ch0_column_num<<") "<<"("<<ch0_row_num<<","<<ch0_column_num<<")"<<endl;
            continue;
        }
    
        int direction;
        int ch1_row_num,ch1_column_num,chn_row_num,chn_column_num;
        bool ch1_find = false;
        bool chn_find = false;
        if( mode == "WRAP" ){
        //search the 2nd chatacter in 8 different direction, and get the search direction
            for( direction = 0; direction < 8 ; ++ direction ){
    	        ch1_row_num = ch0_row_num + position[direction][1];
                ch1_column_num = ch0_column_num + position[direction][0];
                if( ch1_row_num == -1 ){
                    ch1_row_num = row_num - 1;
                }
                if( ch1_row_num == row_num ){
                    ch1_row_num = 0;
                }
                if( ch1_column_num == -1 ){
                    ch1_column_num = column_num - 1;
                }
                if( ch1_column_num == column_num){
                    ch1_column_num = 0;
                }
                if( grid[ch1_row_num][ch1_column_num] == word[i][1] ){
                    grid[ch1_row_num][ch1_column_num] = '\0';
                    ch1_find = true;
                    break;
                }
            }
            //not found the 1st character
            if( ch1_find == false ){
                cout<<"NOT FOUND"<<endl;
                continue;
            }
            //the word length is 2
            if( ch1_find == true && word[i].size() == 2 ){
                cout<<"("<<ch0_row_num<<","<<ch0_column_num<<") "<<"("<<ch1_row_num<<","<<ch1_column_num<<")"<<endl;
                continue;
            }
            //based on search direction, search from the 3rd character
            for(unsigned int chn = 2; chn < word[i].size() ;++ chn ){
                chn_row_num = ch1_row_num + position[direction][1];
                chn_column_num = ch1_column_num + position[direction][0];
                if( chn_row_num == -1 ){
                    chn_row_num = row_num - 1;
                }
                if( chn_row_num == row_num ){
                    chn_row_num = 0;
                }
                if( chn_column_num == -1 ){
                    chn_column_num = column_num - 1;
                }
                if( chn_column_num == column_num){
                    chn_column_num = 0;
                }
                if( grid[chn_row_num][chn_column_num] == word[i][chn] ){
                    grid[chn_row_num][chn_column_num] = '\0';
                    chn_find = true;
                }else{
                    chn_find = false;
                    break;
                }
            }
            if( chn_find == true ){
                cout<<"("<<ch0_row_num<<","<<ch0_column_num<<") "<<"("<<chn_row_num<<","<<chn_column_num<<")"<<endl;
            }else{
                cout<<"NOT FOUND"<<endl;
            }
        }else if( mode == "NO_WRAP" ){
            //search the 2nd chatacter in 8 different direction, and get the search direction
            for( direction = 0 ; direction < 8 ; ++ direction ){
                ch1_row_num = ch0_row_num + position[direction][1];
                ch1_column_num = ch0_column_num + position[direction][0];
                if( ch1_row_num >= 0 && ch1_row_num <= row_num-1
                    && ch1_column_num >= 0 && ch1_column_num <= column_num-1
                    && grid[ch1_row_num][ch1_column_num] == word[i][1] )
                { 
    		grid[ch1_row_num][ch1_column_num] = '\0';
                    ch1_find = true;
                    break;
                }
            }
            //not found the 1st character
            if( ch1_find == false ){
                cout<<"NOT FOUND"<<endl;
                continue;
            }
            //the word length is 2
            if( ch1_find == true && word[i].size() == 2 ){
                cout<<"("<<ch0_row_num<<","<<ch0_column_num<<") "<<"("<<ch1_row_num<<","<<ch1_column_num<<")"<<endl;
                continue;
            }
            //based on search direction, search from the 3rd character
            for(unsigned int chn = 2; chn < word[i].size() ;++ chn ){
                chn_row_num = ch1_row_num + position[direction][1];
                chn_column_num = ch1_column_num + position[direction][0];
                if( chn_row_num >= 0 && chn_row_num <= row_num-1
                    && chn_column_num >= 0 && chn_column_num <= column_num-1 
                    && grid[chn_row_num][chn_column_num] == word[i][chn] )
                {
                    grid[chn_column_num][chn_column_num] = '\0';
                    chn_find = true;
                }else{
                    chn_find = false;
                    break;
                }
            }
            if( chn_find == true ){
                cout<<"("<<ch0_row_num<<","<<ch0_column_num<<") "<<"("<<chn_row_num<<","<<chn_column_num<<")"<<endl;
            }else{
                cout<<"NOT FOUND"<<endl;
            }
        }
    }
    return 0;
}
